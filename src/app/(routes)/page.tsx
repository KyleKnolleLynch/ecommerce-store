import Container from '@/components/ui/container'
import getBillboard from '@/actions/get-billboard'
import Billboard from '@/components/billboard'
import getProducts from '@/actions/get-products'
import ProductList from '@/components/product-list'

export const revalidate = 0

const HomePage = async () => {
  const products = await getProducts({ isFeatured: true })
  const billboard = await getBillboard('a2dac134-bdcf-4e7c-bc44-ee7528a14daa')
  return (
    <main>
      <Container>
        <section className='space-y-10'>
          <Billboard data={billboard} />
        </section>
        <section className='flex flex-col gap-y-8 px-4 sm:px-6 lg:px-8'>
          <ProductList title='Featured Products' items={products} />
        </section>
      </Container>
    </main>
  )
}
export default HomePage
