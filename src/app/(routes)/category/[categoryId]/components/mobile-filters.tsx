'use client'

import { useState } from 'react'
import { Dialog } from '@headlessui/react'
import { Plus, X } from 'lucide-react'
//  local
import { Color, Size } from '@/types'
import Button from '@/components/ui/button'
import IconButton from '@/components/ui/icon-button'
import Filter from './filter'

interface MobileFiltersProps {
  sizes: Size[]
  colors: Color[]
}

const MobileFilters: React.FC<MobileFiltersProps> = ({ sizes, colors }) => {
  const [open, setOpen] = useState(false)

  const onOpen = () => setOpen(true)
  const onClose = () => setOpen(false)

  return (
    <>
      <Button onClick={onOpen} className='flex items-center gap-x-2 lg:hidden'>
        Filters
        <Plus size={20} />
      </Button>

      <Dialog
        open={open}
        onClose={onClose}
        as='div'
        className='relative z-40 lg:hidden'
      >
        <div className='fixed inset-0 bg-black bg-opacity-25' />

        <div className='fixed inset-0 z-40 flex'>
          <Dialog.Panel className='w-full max-w-xs h-full relative flex flex-col overflow-y-auto bg-white  ml-auto py-4 shadow-xl'>
            <div className='flex items-center justify-end px-4'>
              <IconButton icon={<X size={15} />} onClick={onClose} />
            </div>

            <div className='p-4'>
              <Filter valueKey='sizeId' name='Sizes' data={sizes} />
              <Filter valueKey='colorId' name='Colors' data={colors} />
            </div>
          </Dialog.Panel>
        </div>
      </Dialog>
    </>
  )
}
export default MobileFilters
