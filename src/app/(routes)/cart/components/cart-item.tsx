'use client'

import Image from 'next/image'
import { toast } from 'react-hot-toast'
import { X } from 'lucide-react'

//  local
import IconButton from '@/components/ui/icon-button'
import Currency from '@/components/ui/currency'
import useCart from '@/hooks/use-cart'
import { Product } from '@/types'

interface CartItemProps {
  data: Product
}

const CartItem: React.FC<CartItemProps> = ({ data }) => {
    const cart = useCart()

    const onDelete = () => {
        cart.removeItem(data.id)
    }

  return (
    <li className='flex py-6 border-b'>
      <div className='w-24 h-24 rounded-md sm:w-48 sm:h-48 overflow-hidden relative'>
        <Image
          src={data.images[0].url}
          alt='Cart Item'
          fill
          className='object-cover object-center'
        />
      </div>
      <div className='flex flex-1 flex-col justify-between relative sm:ml-6'>
        <div className='absolute right-0 top-0 z-10'>
          <IconButton onClick={onDelete} icon={<X size={15} />} />
        </div>
        <div className="pr-9 sm:grid sm:grid-cols-2 sm:gap-x-6 sm:pr-0 relative">
            <div className="flex justify-between">
                <p className="text-lg text-semibold text-black">
                    {data.name}
                </p>
            </div>

            <div className="mt-1 flex text-sm">
                <p className='text-gray-500'>{data.color.name}</p>
                <p className='text-gray-500 ml-4 pl-4 border-l border-gray-200'>{data.size.value}</p>
            </div>
            <Currency value={data.price} />
        </div>
      </div>
    </li>
  )
}

export default CartItem
