'use client'

import { useEffect, useState } from 'react'
//  local
import Container from '@/components/ui/container'
import useCart from '@/hooks/use-cart'
import CartItem from './components/cart-item'
import Summary from './components/summary'

const CartPage = () => {
  const [isMounted, setIsMounted] = useState(false)
  const cart = useCart()

  useEffect(() => {
    setIsMounted(true)
  }, [])

  if (!isMounted) return null

  return (
    <main className='bg-white'>
      <Container>
        <div className='px-4 py-16 sm:px-6 lg:px-8'>
          <h1 className='text-3xl font-bold'>Shopping Cart</h1>
          <div className='mt-12 lg:grid lg:grid-cols-12 gap-x-12'>
            <section className='lg:col-span-7'>
              {cart.items.length === 0 && (
                <p className='text-neutral-500'>Cart is empty.</p>
              )}
              <ul>
                {cart.items.map(item => (
                  <CartItem key={item.id} data={item} />
                ))}
              </ul>
            </section>
            <Summary />
          </div>
        </div>
      </Container>
    </main>
  )
}
export default CartPage
