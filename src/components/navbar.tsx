import Link from 'next/link'

//  local
import Container from '@/components/ui/container'
import PrimaryNav from '@/components/primary-nav'
import NavbarActions from '@/components/navbar-actions'
import getCategories from '@/actions/get-categories'

const Navbar = async () => {
  const categories = await getCategories()
  return (
    <header className='border-b'>
      <Container>
        <div className='h-16 px-4 sm:px-6 lg:px-0 relative flex items-center'>
          <Link href='/' className='ml-4 lg:ml-0 flex gap-x-2'>
            <p className='font-bold text-xl'>STORE</p>
          </Link>
          <PrimaryNav data={categories} />
          <NavbarActions />
        </div>
      </Container>
    </header>
  )
}
export default Navbar
