'use client'

import Image from 'next/image'
import { Tab } from '@headlessui/react'

import { Image as ImageType } from '@/types'
import GalleryTab from './gallery-tab'

interface GalleryProps {
  images: ImageType[]
}

const Gallery: React.FC<GalleryProps> = ({ images }) => {
  return (
    <Tab.Group as='div' className='flex flex-col-reverse'>
      <div className='w-full max-w-2xl mx-auto mt-6 hidden sm:block lg:max-w-none'>
        <Tab.List className='grid grid-cols-4 gap-6'>
          {images.map(img => (
            <GalleryTab key={img.id} image={img} />
          ))}
        </Tab.List>
      </div>
      <Tab.Panels className='w-full aspect-square'>
        {images.map(img => (
          <Tab.Panel key={img.id}>
            <div className='w-full h-full aspect-square sm:rounded-lg overflow-hidden relative'>
              <Image
                src={img.url}
                alt='Gallery Image'
                fill
                className='object-cover object-center'
                sizes="(min-width: 640px) 25vw, 100vw"
              />
            </div>
          </Tab.Panel>
        ))}
      </Tab.Panels>
    </Tab.Group>
  )
}
export default Gallery
