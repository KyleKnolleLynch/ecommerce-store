import Image from 'next/image'
import { Tab } from '@headlessui/react'

//  local
import { cn } from '@/lib/utils'
import { Image as ImageType } from '@/types'

interface GalleryTabProps {
  image: ImageType
}

const GalleryTab: React.FC<GalleryTabProps> = ({ image }) => {
  return (
    <Tab className='flex items-center justify-center aspect-square rounded-md bg-white cursor-pointer relative'>
      {({ selected }) => (
        <div>
          <span className='absolute w-full h-full aspect-square inset-0 overflow-hidden rounded-md'>
            <Image
              src={image.url}
              alt='Gallery Item'
              fill
              className='object-cover object-center'
              sizes='10vw'
            />
            <span
              className={cn(
                'absolute inset-0 rounded-md ring-2 ring-offset-2',
                selected ? 'ring-black' : 'ring-transparent'
              )}
            />
          </span>
        </div>
      )}
    </Tab>
  )
}
export default GalleryTab
