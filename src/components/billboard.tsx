import { Billboard as BillboardType } from '@/types'
import Image from 'next/image'

interface BillboardProps {
  data: BillboardType
}

const Billboard: React.FC<BillboardProps> = async ({ data }) => {
  return (
    <section className='p-4 sm:p-6 lg:p-8 rounded-xl overflow-hidden'>
      <div className='rounded-xl relative aspect-square md:aspect-[2.4/1] overflow-hidden'>
        <Image
          src={data?.imageUrl}
          alt='Billboard'
          fill
          className='object-cover object-center'
          priority
          sizes='100vw'
        />
        <span className='absolute inset-0 mix-blend-multiply bg-zinc-300'></span>
        <div className='w-full h-full flex flex-col justify-center items-center text-center gap-y-8 text-white'>
          <p className='font-bold text-3xl sm:text-5xl lg:text-6xl sm:max-w-xl max-w-xs z-10'>
            {data.label}
          </p>
        </div>
      </div>
    </section>
  )
}

export default Billboard
