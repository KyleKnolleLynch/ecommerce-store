const Footer = () => {
  return <footer className='border-t mt-20'>
    <div className="mx-auto py-10">
        <p className="text-center text-sm">
            &copy; 2023 Lorem Ipsum Store, Inc. All rights reserved.
        </p>
    </div>
  </footer>;
};
export default Footer;
