'use client'

import { ShoppingCart } from 'lucide-react'
//  local
import { Product } from '@/types'
import Currency from '@/components/ui/currency'
import Button from '@/components/ui/button'
import useCart from '@/hooks/use-cart'

interface InfoProps {
  data: Product
}

const Info: React.FC<InfoProps> = ({ data }) => {
  const cart = useCart()

  const onAddToCart = () => {
    cart.addItem(data)
  }

  return (
    <div>
      <h1 className='text-3xl font-bold text-gray-900'>{data.name}</h1>
      <div className='flex items-end justify-between mt-3'>
        <div className='text-2xl text-gray-900'>
          <Currency value={data?.price} />
        </div>
      </div>
      <hr className='my-4' />
      <div className='flex items-center gap-x-4 mb-4'>
        <h3 className='font-semibold text-black'>Size:</h3>
        <div>{data?.size?.value}</div>
      </div>
      <div className='flex items-center gap-x-4 mb-4'>
        <h3 className='font-semibold text-black'>Color:</h3>
        <div
          className='w-6 h-6 rounded-full border border-gray-600'
          style={{ backgroundColor: data?.color?.value }}
        />
      </div>
      <div className='flex items-center gap-x-3 mt-10'>
        <Button className='flex items-center gap-x-2' onClick={onAddToCart}>
          Add to Cart
          <ShoppingCart />
        </Button>
      </div>
    </div>
  )
}
export default Info
