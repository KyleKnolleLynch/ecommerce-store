'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'

//  local
import { cn } from '@/lib/utils'
import { Category } from '@/types'

interface PrimaryNavProps {
  data: Category[]
}

const PrimaryNav: React.FC<PrimaryNavProps> = ({ data }) => {
  const pathname = usePathname()

  const routes = data.map(route => ({
    href: `/category/${route.id}`,
    label: route.name,
    active: pathname === `/category/${route.id}`,
  }))

  return (
    <nav
      aria-label='Primary'
      className='mx-6 flex items-center space-x-4 lg:space-x-6'
    >
      {routes.map(route => (
        <Link
          key={route.href}
          href={route.href}
          className={cn(
            'text-sm font-medium transition-colors ',
            route.active ? 'text-black' : 'text-neutral-500'
          )}
        >
          {route.label}
        </Link>
      ))}
    </nav>
  )
}
export default PrimaryNav
