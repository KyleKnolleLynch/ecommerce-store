import { MouseEventHandler } from 'react'
//  local
import { cn } from '@/lib/utils'

interface IconButtonProps {
  onClick?: MouseEventHandler<HTMLButtonElement> | undefined
  icon: React.ReactElement
  className?: string
}

const IconButton: React.FC<IconButtonProps> = ({
  onClick,
  icon,
  className,
}) => {
  return (
    <button
      onClick={onClick}
      className={cn(
        'p-2 flex items-center justify-center bg-white rounded-full border shadow-md hover:scale-110 transition',
        className
      )}
    >
      {icon}
    </button>
  )
}
export default IconButton
