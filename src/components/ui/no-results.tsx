const NoResults = () => {
  return (
    <div className='w-full h-full flex items-center justify-center text-neutral-500'>
      No results found.
    </div>
  )
}
export default NoResults
