'use client'

import { MouseEventHandler } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { Expand, ShoppingCart } from 'lucide-react'
//  local
import { Product } from '@/types'
import IconButton from '@/components/ui/icon-button'
import Currency from '@/components/ui/currency'
import usePreviewModal from '@/hooks/use-preview-modal'
import useCart from '@/hooks/use-cart'

interface ProductCardProps {
  data: Product
}

const ProductCard: React.FC<ProductCardProps> = ({ data }) => {
  const router = useRouter()
  const cart = useCart()
  const previewModal = usePreviewModal()

  const handleClick = () => {
    router.push(`/product/${data?.id}`)
  }

  const onPreview: MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation()

    previewModal.onOpen(data)
  }

  const onAddToCart: MouseEventHandler<HTMLButtonElement> = e => {
    e.stopPropagation()

    cart.addItem(data)
  }

  return (
    <article
      onClick={handleClick}
      className='p-3 space-y-4 bg-white group rounded-xl border cursor-pointer'
    >
      <div className='aspect-square rounded-xl bg-gray-100 relative'>
        <Image
          src={data?.images?.[0]?.url}
          alt='Product'
          fill
          className='aspect-square rounded-md object-cover'
          sizes='(min-width: 640px) 50vw, (min-width: 768px) 33vw, (min-width: 1024px) 25vw, 100vw'
        />
        <div className='opacity-0 group-hover:opacity-100 transition absolute w-full px-6 bottom-5'>
          <div className='flex gap-x-6 justify-center'>
            <IconButton
              onClick={onPreview}
              icon={<Expand size={20} className='text-gray-600' />}
            />
            <IconButton
              onClick={onAddToCart}
              icon={<ShoppingCart size={20} className='text-gray-600' />}
            />
          </div>
        </div>
      </div>
      <div>
        <p className='font-semibold text-lg'>{data.name}</p>
        <p className='text-sm text-gray-500'>{data.category?.name}</p>
      </div>
      <div className='flex items-center justify-between'>
        <Currency value={data.price} />
      </div>
    </article>
  )
}
export default ProductCard
