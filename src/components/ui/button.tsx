import { forwardRef } from 'react'
//  local
import { cn } from '@/lib/utils'

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ className, children, disabled, type = 'button', ...props }, ref) => {
    return (
      <button
        ref={ref}
        {...props}
        className={cn(
          `
            w-auto 
            px-5
            py-3
            rounded-full
            bg-black
            text-white
            border-transparent
            font-semibold
            disabled:cursor-not-allowed
            disabled:opacity-50
            hover:opacity-75
            transition
        `,
          className
        )}
        disabled={disabled}
      >
        {children}
      </button>
    )
  }
)

Button.displayName = 'Button'

export default Button
