# Ecommerce Store

 ### 🔗 [Live website](https://ecommerce-store-one-indol.vercel.app/)

- Nextjs with Typescript

- Headless UI

- TailwindCSS

- Zustand

- Lucide React Icons

This e-commerce website project lets the user shop by main categories, then optionally filter products by size or color once a category has been selected. Featured items on the home page can also be selected directly. Items can be added or removed from the cart, then checkout and payment is handled by Stripe implementation.

###### * *(Note: This app is strictly a non-commercial, personal project. Stripe is in testing mode only, no real payments or money exchange takes place. To make a mock checkout, use dummy info and type 4242 4242 4242 4242 as a credit card number.)* *

The products displayed on this application are uploaded and pulled in from an admin dashboard project of mine that can be seen here:

### 🔗 [Live admin dashboard](https://ecommerce-dashboard-beta.vercel.app)

### 🔗 [Source code for admin dashboard](https://gitlab.com/KyleKnolleLynch/ecommerce-dashboard)

Credit and big thanks to [Antonio Erdeljac](https://www.youtube.com/watch?v=5miHyP6lExg&t=1s) for the great tutorial and codebase for this project.

This is a learning project for me, not something I claim to have built myself. But I wanted to document the experience. I completed and figured out as much as I could on my own as I worked through this tutorial. I enjoyed learning and practicing new tech, and tech I'm still building my skills with, like typescript, zod, shadcn/ui, zustand, and prisma in such a large and complex application.